## Project is discontinued
Please don't install this add-on anymore. If you already have it installed, then remove it. All associated files will be deleted soon. We suggest to use the official Kodi Youtube Add-on instead and to directly subscribe to the Youtube Channel of Hacklabor.


## What is (or was) this about?
This is the Hacklabor Kodi repository add-on which provides up-to-date add-ons from Hacklabor.


## Installation
Please remove older versions (earlier than 0.0.2) of the repository add-on first.

Important: you need version 0.0.4 for Kodi 19 (Matrix) support!


Provide the zip file in a share that is accessible for your Kodi installation and do "Install from zip file".

Afterwards you can install add-ons via "Install from repository".

![](screenshots/hacklabor_repo-01.png)

![](screenshots/hacklabor_repo-02.png)

![](screenshots/hacklabor_repo-03.png)

![](screenshots/hacklabor_repo-04.png)

![](screenshots/hacklabor_repo-05.png)

![](screenshots/hacklabor_repo-06.png)

![](screenshots/hacklabor_repo-07.png)

![](screenshots/hacklabor_repo-08.png)

![](screenshots/hacklabor_repo-09.png)

![](screenshots/hacklabor_repo-10.png)


## Copyrights / credits

### Icons
The Hacklabor logo as shown in screenshot files is property of Hackspace Schwerin e.V. and may only be used as element of this add-on. Any other use of it must be permitted by its [owners](mailto:info@hacklabor.de) before.
